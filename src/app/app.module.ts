import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { OrganizerComponent } from './components/organizer/organizer.component';
import { MomentPipe } from './shared/moment.pipe';
import { StringMomentPipe } from './shared/stringMoment.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TaskService } from './shared/task.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MonthSelectorComponent } from './components/month-selector/month-selector.component';
import { DaySelectorComponent } from './components/day-selector/day-selector.component';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskAddComponent } from './components/task-add/task-add.component';
import { MatInputModule } from '@angular/material/input';
import { TextFieldModule } from '@angular/cdk/text-field';
import { MatRippleModule } from '@angular/material/core';
import { LongPressDirective } from './shared/long-press.directive';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { StateService } from './shared/state.service';
import { TaskListEditDialogComponent } from './components/task-list-edit-dialog/task-list-edit-dialog.component';
import { Num2stringPipe } from './shared/num2string.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    OrganizerComponent,
    MomentPipe,
    StringMomentPipe,
    MonthSelectorComponent,
    DaySelectorComponent,
    TaskListComponent,
    TaskAddComponent,
    LongPressDirective,
    TaskListEditDialogComponent,
    Num2stringPipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatInputModule,
    TextFieldModule,
    MatRippleModule,
    StorageServiceModule,
    MatDialogModule,
    MatButtonModule,
  ],
  providers: [
    TaskService,
    StateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
