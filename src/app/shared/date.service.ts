import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { StateService } from './state.service';

@Injectable({
  providedIn: 'root',
})

export class DateService {
  public date: BehaviorSubject<moment.Moment> = new BehaviorSubject<moment.Moment>(moment());
  public month: BehaviorSubject<moment.Moment> = new BehaviorSubject<moment.Moment>(moment());

  changeMonth(direction: number) {
    const value = this.month.value.clone().add(direction, 'month');
    this.month.next(value);
  }

  changeDay(direction: number) {
    const date = this.date.value.add(direction, 'day');
    this.date.next(date);
    this.month.next(date);
  }

  setDate(date: moment.Moment | string) {
    if (moment.isMoment(date)) {
      this.date.next(date);
      this.month.next(date);
    }
    if (typeof date === 'string') {
      const dateFromString = moment(date);
      this.date.next(dateFromString);
      this.month.next(dateFromString);
    }
  }

  isToday(): boolean {
    return this.date.value.isSame(moment(), 'day');
  }

  isTodayOrAfter(): boolean {
    return this.date.value.isSameOrAfter(moment(), 'day');
  }
}
