import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'num2string',
  pure: false,
})
export class Num2stringPipe implements PipeTransform {

  transform(value: number, textForms: string[]): unknown {
    value = Math.abs(value) % 100;
    const n1 = value % 10;

    if (value > 10 && value < 20) {
        return textForms[2];
    }

    if (n1 > 1 && n1 < 5) {
        return textForms[1];
    }

    if (n1 === 1) {
        return textForms[0];
    }

    return textForms[2];
  }

}
