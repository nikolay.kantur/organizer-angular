import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'stringMoment',
  pure: false,
})

export class StringMomentPipe implements PipeTransform {
  transform(str: string, format: string = 'MMMM YYYY'): string {
    const m = moment(str);
    m.locale('ru');
    return m.format(format);
  }
}
