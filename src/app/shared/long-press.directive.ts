import { Directive, ElementRef, HostListener, Input, Output, EventEmitter, HostBinding } from '@angular/core';

@Directive({
  selector: '[appLongPress]'
})
export class LongPressDirective {

  @Input() cdkDragStartDelay = 500;

  @Output() longPress = new EventEmitter();
  @Output() longPressEnd = new EventEmitter();

  private pressingClass: boolean;
  private longPressingClass: boolean;

  private mouseTimer;

  // (mousedown)="cdkDragStartDelay = 0" // TODO move this inside directive

  @HostBinding('class.press')
  get classPress() { return this.pressingClass; }

  @HostBinding('class.longpress')
  get classLongPress() { return this.longPressingClass; }

  @HostListener('touchstart', ['$event'])
  touchstart(event) {
    this.pressingClass = true;
    this.mouseTimer = window.setTimeout(() => {
      this.longPress.emit(event);
      this.pressingClass = false;
      this.longPressingClass = true;
    }, this.cdkDragStartDelay);
  }

  @HostListener('touchmove', ['$event'])
  touchmove(event) {
    if (this.mouseTimer) { window.clearTimeout(this.mouseTimer); }
    this.pressingClass = false;
  }

  @HostListener('touchend', ['$event'])
  touchend(event) {
    if (this.mouseTimer) { window.clearTimeout(this.mouseTimer); }
    this.pressingClass = false;
    this.endLongPress(event);
  }

  @HostListener('touchcancel', ['$event'])
  touchcancel(event) { console.log('touchcancel'); }

  @HostListener('cdkDragStarted', ['$event'])
  cdkDragStarted(event) {
    this.pressingClass = false;
    this.longPressingClass = false;
  }

  endLongPress(event) {
    if (this.longPressingClass) {
      this.longPressingClass = false;
      this.longPressEnd.emit(event);
    }
  }
}