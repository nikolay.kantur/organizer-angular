import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import * as moment from 'moment';

export interface Task {
  _id?: any;
  id?: string;
  title: string;
  date?: string;
  done?: boolean;
  order?: number;
}

@Injectable()
export class TaskService {
  static baseUrl = 'http://192.168.1.46:3019';
  static url = 'http://192.168.1.46:3019/task';

  constructor(
    private http: HttpClient,
  ) {}

  create(task: Task) {
    console.log('service create', task);
    return this.http.post<Task | null>(TaskService.url, task)
      .pipe(map(res => {
        console.log({res});
        return res;
      }));
  }

  getAll(date?: string): Observable<Task[] | null> {
    // TODO change for default method for creating URLs
    const url = `${TaskService.url}${date ? `?date=${date}` : ''}`;
    console.log('service getAll', url);
    return this.http.get<Task[] | null>(url)
      .pipe(
        map(tasks => tasks.sort(this.sortByOrder)),
        map(tasks => tasks.sort(this.sortByDate)),
        tap(data => console.log('getAll', data)),
      );
  }

  sortByOrder(a, b) {
    return a.order - b.order;
  }

  getExpiredTasks() {
    const beforeDate = moment().format('YYYY-MM-DD');
    const url = `${TaskService.url}?beforeDate=${beforeDate}&done=false`;
    console.log('service getExpiredTasks', url);
    return this.http.get<Task[] | null>(url)
      .pipe(
        map(tasks => tasks.sort(this.sortByDate)),
        map(tasks => tasks.sort(this.sortByDateOrder.bind(this))),
        tap(data => console.log('getExpired', data)),
      );
  }

  sortByDate(a, b) {
    const af = moment(a.date).format('YYYY-MM-DD');
    const bf = moment(b.date).format('YYYY-MM-DD');
    return af < bf ? -1 : af === bf ? 0 : 1;
  }

  sortByDateOrder(a, b) {
    if (this.sortByDate(a, b)) {
      return 0;
    }

    return this.sortByOrder(a, b);
  }

  delete(id) {
    console.log('service deleteById', id);
    return this.http.delete(`${TaskService.url}/${id}`)
      .pipe(
        tap(data => console.log('deleteById', data)),
      );
  }

  update(task: Task) { // TODO change this to put/replace
    return this.http.put(`${TaskService.url}/${task._id}`, task)
      .pipe(
        tap(data => console.log('update', data)),
      );
  }

  patch(partial: Partial<Task>) { // TODO change this to update
    return this.http.patch(`${TaskService.url}/${partial._id}`, partial)
      .pipe(
        tap(data => console.log('patch', data)),
      );
  }

  getMonthByDaysHasTasks(date: moment.Moment) {
    const day = date.format('YYYY-MM-DD');
    const url = `${TaskService.url}/month-by-days-has-tasks?day=${day}`;
    console.log('getMonthByDaysHasTasks', url);
    return this.http.get(url)
      .pipe(
        map((data: any[]) => data.map(d => {
            d.date = d._id;
            delete d._id;
            return d;
          })
        ),
        tap(data => console.log('getMonthByDaysHasTasks', data)),
      );
  }
}
