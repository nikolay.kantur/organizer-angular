import { Injectable, Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { BehaviorSubject } from 'rxjs';
import * as moment from 'moment';
import { DateService } from './date.service';

const STORAGE_KEY = 'state';

@Injectable({
  providedIn: 'root'
})
export class StateService {

  showCalendar: BehaviorSubject<boolean> = new BehaviorSubject(false);
  unfoldExpiredTasks: BehaviorSubject<boolean> = new BehaviorSubject(false);
  hideDoneTasks: BehaviorSubject<boolean> = new BehaviorSubject(false); // TODO get from defaults

  constructor(
    @Inject(LOCAL_STORAGE) private storage: StorageService,
    private dateService: DateService,
  ) {
    const data = this.storage.get(STORAGE_KEY);
    this.showCalendar.next(data?.showCalendar || false);
    this.unfoldExpiredTasks.next(data?.unfoldExpiredTasks || false);
    this.hideDoneTasks.next(data?.hideDoneTasks || false);

    this.dateService.setDate(moment(data?.date) || moment());

    this.dateService.date.subscribe(date => this.updateProperty({ date }));
  }

  toggleShowCalendar() {
    const newState = !this.showCalendar.value;
    this.showCalendar.next(newState);
    this.updateProperty({ showCalendar: newState });
  }

  toggleUnfoldExpiredTasks() {
    const newState = !this.unfoldExpiredTasks.value;
    this.unfoldExpiredTasks.next(newState);
    this.updateProperty({ unfoldExpiredTasks: newState });
  }

  toggleHideDoneTasks() {
    const newState = !this.hideDoneTasks.value;
    this.hideDoneTasks.next(newState);
    this.updateProperty({ hideDoneTasks: newState });
  }

  private updateProperty(newData) {
    let data = this.storage.get(STORAGE_KEY);
    data = { ...data, ...newData };
    this.storage.set(STORAGE_KEY, data);
  }
}
