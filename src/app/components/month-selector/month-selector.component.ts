import { Component, OnInit } from '@angular/core';
import { DateService } from '../../shared/date.service';

@Component({
  selector: 'app-month-selector',
  templateUrl: './month-selector.component.html',
  styleUrls: ['./month-selector.component.scss']
})
export class MonthSelectorComponent implements OnInit {

  constructor(
    public dateService: DateService
  ) {}

  ngOnInit(): void {}

}
