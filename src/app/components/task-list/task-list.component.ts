import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Task, TaskService } from 'src/app/shared/task.service';
import { DateService } from 'src/app/shared/date.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Subject } from 'rxjs';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StateService } from 'src/app/shared/state.service';
import { TaskListEditDialogComponent } from '../task-list-edit-dialog/task-list-edit-dialog.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, OnDestroy {

  tasks: Task[] = [];
  tasksDone: Task[] = [];
  tasksExpired: Task[] = [];

  cdkDragStartDelay = 250;
  unsubscribe$: Subject<any> = new Subject<any>();

  constructor(
    public taskService: TaskService,
    public dateService: DateService,
    public stateService: StateService,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.dateService.date
      .subscribe(date => {
        const dateString = date.format('YYYY-MM-DD');
        this.taskService.getAll(dateString).subscribe(tasks => {
          tasks.sort((a, b) => a.order - b.order); // TODO sorting via other rules
          this.tasks = [];
          this.tasksDone = [];
          tasks.forEach(task => {
            if (task.done) {
              this.tasksDone.push(task);
            } else {
              this.tasks.push(task);
            }
          });
        });

        if (this.dateService.isTodayOrAfter()) {
          this.tasksExpired = [];
          this.taskService.getExpiredTasks()
            .subscribe(tasks => this.tasksExpired = tasks);
        }
      });
  }

  delete(task: Task) {
    this.tasks = this.tasks.filter(t => t._id !== task._id);
    this.tasksDone = this.tasksDone.filter(t => t._id !== task._id);
    this.tasksExpired = this.tasksExpired.filter(t => t._id !== task._id);
    this.taskService.delete(task._id).subscribe(deletedTask =>
      this.dateService.changeDay(0) // TODO change this crutch to something more suitable
    );
  }

  edit(task: Task) {
    const dialogRef = this.dialog.open(TaskListEditDialogComponent, {
      width: '600px',
      data: task,
    }).afterClosed().subscribe(result => {
      if (result?.delete) {
        this.delete(task);
      }
      if (result?.save) {
        if (result.task) {
          const updatedTask = result.task;
          console.log({ updatedTask });
          this.taskService.update(updatedTask)
            .subscribe(createdTask => {
              this.dateService.changeDay(0); // TODO change this crutch to something more suitable
            });
        }
      }
    });
  }

  done(task: Task, index: number) {
    this.taskService.update(task).subscribe(updatedTask =>
      this.dateService.changeDay(0) // TODO change this crutch to something more suitable
    );
    if (task.done) {
      this.tasks.splice(index, 1);
      this.tasksDone.push(task);
    }
    if (!task.done) {
      this.tasksDone.splice(index, 1);
      this.tasks.push(task);
    }

    this.reorder();
  }

  drop(event: CdkDragDrop<string[]>) {
    let updatePage = false;
    console.log({ event });
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      this.reorder();
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);
      this.tasks.forEach((task, index) => {
        let needToUpdate = false;
        if (task.done) {
          task.done = false;
          needToUpdate = true;
          updatePage = true;
        }
        if (task.order !== index) {
          task.order = index;
          needToUpdate = true;
        }
        if (task.date !== this.dateService.date.value.format('YYYY-MM-DD')) {
          task.date = this.dateService.date.value.format('YYYY-MM-DD');
          needToUpdate = true;
        }
        if (needToUpdate) {
          this.taskService.update(task).subscribe();
        }
      });
      this.tasksDone.forEach((task, index) => {
        let needToUpdate = false;
        if (!task.done) {
          task.done = true;
          needToUpdate = true;
          updatePage = true;
        }
        if (task.order !== index) {
          task.order = index;
          needToUpdate = true;
        }
        if (task.date !== this.dateService.date.value.format('YYYY-MM-DD')) {
          task.date = this.dateService.date.value.format('YYYY-MM-DD');
          needToUpdate = true;
        }
        if (needToUpdate) {
          this.taskService.update(task).subscribe();
        }
      });
    }

    if (updatePage) { this.dateService.changeDay(0); } // TODO change this crutch to something more suitable
  }

  private reorder() {
    this.tasks.forEach(this.checkOrder.bind(this));
    this.tasksDone.forEach(this.checkOrder.bind(this));
  }

  private checkOrder(task, index) {
    if (task.order !== index) {
      task.order = index;
      this.taskService.update(task).subscribe();
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
