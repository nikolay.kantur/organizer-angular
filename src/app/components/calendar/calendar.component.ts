import { Component, OnInit, ElementRef, Renderer2, OnDestroy } from '@angular/core';
import * as moment from 'moment';
import { DateService } from '../../shared/date.service';
import { CdkDragDrop, CdkDragRelease, CdkDragEnter, CdkDragExit } from '@angular/cdk/drag-drop';
import { TaskService } from 'src/app/shared/task.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface Day {
  value: moment.Moment;
  active: boolean;
  disabled: boolean;
  selected: boolean;
}

interface Week {
  days: Day[];
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnDestroy {

  calendar: Week[];

  unsubscribe$: Subject<any> = new Subject<any>();

  private date;
  private month;
  private daysWithTasks: any[] = [];

  constructor(
    public dateService: DateService,
    private taskService: TaskService,
    private renderer: Renderer2,
  ) { }

  ngOnInit(): void {
    this.dateService.month
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(month => {
        this.month = month;
        this.generate();
        this.taskService.getMonthByDaysHasTasks(this.month).subscribe(data => {
          this.daysWithTasks = data;
          this.generate(); // TODO remove "double generating"
        });
      });

    this.dateService.date
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(date => {
        this.date = date;
        this.generate();
      });
  }

  private generate() {
    if (!this.date || !this.month) { return ; }

    const startDay = this.month.clone().startOf('month').startOf('isoWeek');
    const endDay = this.month.clone().endOf('month').endOf('isoWeek');

    const date = startDay.clone().subtract(1, 'day');

    const calendar = [];

    while (date.isBefore(endDay, 'day')) {
      calendar.push({
        days: Array(7)
          .fill(0)
          .map(() => {
            const value = date.add(1, 'day').clone();
            const today = moment().isSame(value, 'date');
            const disabled = !this.month.isSame(value, 'month');
            const selected = this.date.isSame(value, 'date');
            const tasks = !!this.daysWithTasks.find(day => day.date === value.format('YYYY-MM-DD'));
            const tasksUndone = !!this.daysWithTasks.find(day => (day.date === value.format('YYYY-MM-DD')) && day.tasksUndone );
            const tasksExpired = tasksUndone && value.isBefore(moment(), 'day');
            const tasksDone = !!this.daysWithTasks.find(day => (day.date === value.format('YYYY-MM-DD')) && day.tasksDone );

            return {
              value, today, disabled, selected, tasks, tasksUndone, tasksExpired, tasksDone
            };
          })
      });
    }

    this.calendar = calendar;
  }

  cdkDropListEntered(event: CdkDragEnter<string[]>) {
    const preview = new ElementRef<HTMLElement>(document.querySelector('.cdk-drag.cdk-drag-preview'));
    this.renderer.addClass(preview.nativeElement, 'no-animation');
  }

  cdkDropListExited(event: CdkDragExit<string[]>) {
    const preview = new ElementRef<HTMLElement>(document.querySelector('.cdk-drag.cdk-drag-preview'));
    this.renderer.removeClass(preview.nativeElement, 'no-animation');
  }

  drop(event: CdkDragDrop<string[]>, day: moment.Moment = null) {
    const id = event.item.element.nativeElement.id;
    const date = day?.format('YYYY-MM-DD'); // TODO move this to some service

    const task = {
      _id: id,
      date,
      order: 1000, // TODO change this crutch
    };

    this.taskService.patch(task)
      .subscribe(updatedTask => {
        this.dateService.changeDay(0); // TODO change this crutch to something more suitable
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }


}
