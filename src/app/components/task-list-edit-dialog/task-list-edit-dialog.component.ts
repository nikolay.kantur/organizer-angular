import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Task } from 'src/app/shared/task.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-task-list-edit-dialog',
  templateUrl: './task-list-edit-dialog.component.html',
  styleUrls: ['./task-list-edit-dialog.component.scss']
})
export class TaskListEditDialogComponent implements OnInit {
  form: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<TaskListEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public task: Task,
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(this.task.title, Validators.required)
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  delete(): void {
    this.dialogRef.close({ delete: true });
  }

  save(): void {
    this.task.title = this.form.value.title;
    this.dialogRef.close({
      save: true,
      task: this.task,
    });
  }

}
