import { Component, OnInit, OnDestroy } from '@angular/core';
import { DateService } from '../../shared/date.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { StateService } from 'src/app/shared/state.service';


@Component({
  selector: 'app-organizer',
  templateUrl: './organizer.component.html',
  styleUrls: ['./organizer.component.scss']
})
export class OrganizerComponent implements OnInit, OnDestroy {

  showCalendar: boolean;

  private unsubscribe$: Subject<any> = new Subject<any>();

  constructor(
    public dateService: DateService,
    public stateService: StateService,
  ) { }

  ngOnInit(): void {
    this.stateService.showCalendar
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(s => this.showCalendar = s);
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
