import { Component, OnInit } from '@angular/core';
import { DateService } from 'src/app/shared/date.service';
import * as moment from 'moment';
import { StateService } from 'src/app/shared/state.service';

@Component({
  selector: 'app-day-selector',
  templateUrl: './day-selector.component.html',
  styleUrls: ['./day-selector.component.scss']
})
export class DaySelectorComponent implements OnInit {


  constructor(
    public dateService: DateService,
    public stateService: StateService,
  ) {}

  ngOnInit(): void {}

  goToday() {
    this.dateService.setDate(moment());
  }

}
