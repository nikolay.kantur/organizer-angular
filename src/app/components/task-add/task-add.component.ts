import { Component, OnInit } from '@angular/core';
import { TaskService, Task } from 'src/app/shared/task.service';
import { DateService } from 'src/app/shared/date.service';

@Component({
  selector: 'app-task-add',
  templateUrl: './task-add.component.html',
  styleUrls: ['./task-add.component.scss']
})
export class TaskAddComponent implements OnInit {

  title = '';

  constructor(
    private taskService: TaskService,
    private dateService: DateService,
  ) { }

  ngOnInit(): void {}

  submit() {
    if (!this.title) { return ; }
    const newTask: Task = {
      title: this.title,
      date: this.dateService.date.value.format('YYYY-MM-DD'), // TODO move this to some service
    };

    this.title = '';

    this.taskService.create(newTask)
      .subscribe(createdTask => {
        this.dateService.changeDay(0); // TODO change this crutch to something more suitable
      });
  }

}
